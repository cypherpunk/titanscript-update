@echo off
IF EXIST TitanScriptRelease rmdir TitanScriptRelease /s /q
mkdir TitanScriptRelease
mkdir .\TitanScriptRelease\x86
mkdir .\TitanScriptRelease\x64

copy .\Release\x32\TitanScriptGui.exe .\TitanScriptRelease\x86\TitanScriptGui.exe
copy .\Release\x32\TitanScriptGui.map .\TitanScriptRelease\x86\TitanScriptGui.map
copy .\Release\x32\TitanUnitTest.exe .\TitanScriptRelease\x86\TitanUnitTest.exe

copy .\Release\x64\TitanScriptGui.exe .\TitanScriptRelease\x64\TitanScriptGui.exe
copy .\Release\x64\TitanScriptGui.map .\TitanScriptRelease\x64\TitanScriptGui.map
copy .\Release\x64\TitanUnitTest.exe .\TitanScriptRelease\x64\TitanUnitTest.exe

mkdir .\TitanScriptRelease\x86\plugins
mkdir .\TitanScriptRelease\x86\plugins\x86
mkdir .\TitanScriptRelease\x64\plugins
mkdir .\TitanScriptRelease\x64\plugins\x64

copy .\Release\x32\TitanScript.dll .\TitanScriptRelease\x86\plugins\x86\TitanScript.dll
copy .\Release\x64\TitanScript.dll .\TitanScriptRelease\x64\plugins\x64\TitanScript.dll

pause

