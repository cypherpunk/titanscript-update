#include "Opcodes.h"

#include "TE_Interface.h"
#include "Debug.h"
#include "HelperFunctions.h"

#include "disasm.h"
//#pragma comment(lib, "disasm.lib")


static t_Assemble _Assemble = 0;

#include "..\TitanEngine\TitanEngine.hpp"

using namespace TE;

size_t Assemble(const string& cmd, rulong ip, byte* op)
{
    size_t size = 0;

#ifndef _WIN64

    if (!_Assemble)
    {
        HMODULE hMod = LoadLibraryW(DISASM_DLL);
        if (hMod)
        {
            _Assemble = (t_Assemble)GetProcAddress(hMod, DISASM_DLL_ASSEMBLE);
            if (!_Assemble)
            {
                MessageBoxW(0, L"Cannot load API Assemble from library " DISASM_DLL, L"ERROR", MB_ICONWARNING);
                FreeLibrary(hMod);
                return 0;
            }
        }
        else
        {
            MessageBoxW(0, L"Cannot load library " DISASM_DLL, L"ERROR", MB_ICONWARNING);
            return 0;
        }
    }

    t_asmmodel info;
    char error[TEXTLEN];

    if(0 < _Assemble((char*)cmd.c_str(), ip, &info, 0, 3, error))
    {
        size = info.length;
        memcpy(op, info.code, info.length);
    }
#endif
    return size;
}

size_t AssembleEx(const string& cmd, rulong ip)
{
    byte op[MAX_INSTR_SIZE];
    size_t size;

    if((size = Assemble(cmd, ip, op)) && TE_WriteMemory(ip, size, op))
    {
        return size;
    }
    return 0;
}

string Disassemble(const byte* op, rulong ip, size_t* size)
{
    size_t cmdsize = 0;
    string cmd = "";


    //t_disasm info;
    //cmdsize = Disasm((char*)op, MAXCMDSIZE, ip, &info, DISASM_FILE);
    //cmd = info.result;

    const char* instr = Debugger::StaticDisassembleEx(ip, op);
    if(instr)
    {
        cmd = instr;
        if(size)
            cmdsize = Debugger::StaticLengthDisassemble(op);
    }

    if(size) *size = cmdsize;
    return cmd;
}

string DisassembleEx(rulong ip, size_t* size)
{
    byte op[MAX_INSTR_SIZE];

    if(TE_ReadMemory(ip, sizeof(op), &op))
    {
        return Disassemble(op, ip, size);
    }
    else
    {
        if(size) *size = 0;
        return "";
    }
}

size_t LengthDisassemble(const byte* op)
{
    return Debugger::StaticLengthDisassemble(op);
}

size_t LengthDisassemble(const string& cmd)
{
    byte op[MAX_INSTR_SIZE];

    return Assemble(cmd, 0, op);
}

size_t LengthDisassembleEx(rulong ip)
{
    byte op[MAX_INSTR_SIZE];
    if(TE_ReadMemory(ip, sizeof(op), &op))
    {
        return LengthDisassemble(op);
    }
    return 0;
}

size_t LengthDisassembleBack(const byte* op)
{
    byte tmp[2*MAX_INSTR_SIZE] = {0};
    size_t offs = 0;

    memcpy(tmp, op, MAX_INSTR_SIZE);

    while(offs < MAX_INSTR_SIZE)
    {
        size_t cmdsize = LengthDisassemble(tmp + offs);
        offs += cmdsize;
        if(offs == MAX_INSTR_SIZE)
        {
            return cmdsize;
        }
    }
    return 0;
}

size_t LengthDisassembleBackEx(rulong ip)
{
    byte op[MAX_INSTR_SIZE];

    if(TE_ReadMemory(ip - sizeof(op), sizeof(op), &op))
    {
        return LengthDisassembleBack(op);
    }
    return 0;
}
